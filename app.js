const express = require('express');

const app = express();

app.get('/', (req, res) => {
   res.send('Server is running !!!! updated');
});

app.get('/health-check', (req, res) => {
   res.send('Server is running on port 3000');
});

app.listen(3000, () => console.log('server is running on port 3000'));
